import unittest
import requests
from configparser import ConfigParser


class TestFlaskMain(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestFlaskMain, self).__init__(*args, **kwargs)
        config = ConfigParser()
        config.read("./config.ini")

        self.server_config = config["server"]

    def send_request_to_server(self, path, streaming=False):
        return requests.get("{}://{}:{}/{}".format(self.server_config["protocol"],
                                                   self.server_config["host"],
                                                   self.server_config["port"],
                                                   path), stream=streaming)

    def test_hello_world(self):
        response = self.send_request_to_server("")

        self.assertEqual(response.text, "Hello World!")

    def test_data_stream(self):
        response = self.send_request_to_server("data_stream", True)

        count = 0
        for message in response.iter_lines(chunk_size=1):
            if message:
                count += 1
                if count == 10:
                    break
        self.assertEqual(count, 10)

    def test_db_status(self):
        response = self.send_request_to_server("db_status")

        assert (response.text == "True" or response.text == "False")

    def test_stat_pnl_realised(self):
        response = self.send_request_to_server("statistics/pnl_realised")

        assert response.text and response.status_code == 200

    def test_login(self):
        response = self.send_request_to_server("login")

        assert (response.status_code == 200 or response.status_code == 401 or response.status_code == 503)
