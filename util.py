import hashlib


class Util:
    @staticmethod
    def hash_with_secret(data, secret):
        return hashlib.sha256((data + secret).encode()).hexdigest()
