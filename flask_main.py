from flask import Flask, Response, request
from configparser import ConfigParser
import requests
from flask_cors import CORS
import json

from util import Util

config = ConfigParser()
config.read("./config.ini")

server_config = config["server"]
data_gen_server_config = config["data-gen-server"]
dao_server_config = config["dao-server"]

app = Flask(__name__)
CORS(app)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/data_stream")
def data_stream():
    resp = requests.get("{}://{}:{}/data_stream".format(data_gen_server_config["protocol"],
                                                        data_gen_server_config["host"],
                                                        data_gen_server_config["port"]), stream=True)

    def event_stream():
        for message in resp.iter_lines(chunk_size=1):
            if message:
                yield "data:" + message.decode("utf-8") + "\n\n"

    return Response(event_stream(), mimetype="text/event-stream")


@app.route("/db_status")
def db_status():
    return requests.get("{}://{}:{}/db_status".format(dao_server_config["protocol"],
                                                      dao_server_config["host"],
                                                      dao_server_config["port"])).content


@app.route("/statistics/pnl_realised")
def stat_pnl_realised():
    return requests.get("{}://{}:{}/statistics/pnl_realised".format(dao_server_config["protocol"],
                                                                    dao_server_config["host"],
                                                                    dao_server_config["port"])).content


@app.route("/login", methods=["POST"])
def login():
    json_obj = json.loads(str(request.json).replace("'", "\""))
    username = json_obj["username"]
    password = json_obj["password"]
    if username and password:
        try:
            resp = requests.get("{}://{}:{}/find_user_password".format(dao_server_config["protocol"],
                                                                       dao_server_config["host"],
                                                                       dao_server_config["port"]),
                                json="{\"username\": \"" + username + "\"}")
        except:
            return Response(status=503)

        password_from_db = resp.content.decode("utf-8")
        hashed_password = Util.hash_with_secret(password, config["secret"]["secret"])
        if hashed_password == password_from_db:
            return Response(status=200)
        else:
            return Response(status=401)

    return Response(status=401)


def boot_app():
    app.run(port=server_config["port"], threaded=True, host=server_config["host"])
